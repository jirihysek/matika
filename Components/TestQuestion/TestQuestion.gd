extends Node2D

var rody = ["mužský rod", "ženský rod", "střední rod"]
var cisla = ["jednotné", "množné"]

var data = [
	["strom", 0],
	["muškáty", 0],
	["keř", 0],
	["list", 0],
	["kořeny", 0],
	["stonek", 0],
	["humus", 0],
	["minerály", 0],
	["kameny", 0],
	["vzduch", 0],
	["kyslík", 0],
	["dusík", 0],
	["oceán", 0],
	["kaštan", 0],
	["rybník", 0],
	["potůček", 0],
	["vítr", 0],
	["kocour", 0],
	["čtyřlístek", 0],
	["jetel", 0],
	["rybíz", 0],
	["hrášek", 0],
	["močál", 0],
	["písek", 0],
	["plody", 0],
	["voda", 1],
	["řeka", 1],
	["žlutnice", 1],
	["hlína", 1],
	["půda", 1],
	["větve", 1],
	["květiny", 1],
	["tráva", 1],
	["kůra", 1],
	["magnolie", 1],
	["ropa", 1],
	["sedmikráska", 1],
	["jiřiny", 1],
	["lilie", 1],
	["žížala", 1],
	["pampeliška", 1],
	["byliny", 1],
	["šiška", 1],
	["malina", 1],
	["borůvky", 1],
	["jahoda", 1],
	["fazole", 1],
	["louka", 1],
	["růže", 1],
	["paprika", 1],
	["zvětrávání", 2],
	["moře", 2],
	["jezero", 2],
	["kůzle", 2],
	["kuře", 2],
	["poupě", 2],
	["jablko", 2],
	["klíště", 2],
	["mraveniště", 2],
	["jehličí", 2],
	["vejce", 2],
	["hnízda", 2],
	["borůvčí", 2],
	["semínka", 2],
	["mláďata", 2],
	["pohoří", 2],
	["strniště", 2],
	["pole", 2],
	["jehně", 2],
	["chmýří", 2],
	["kotě", 2],
	["zvířátka", 2],
	["štěně", 2],
	["hříbata", 2],
	["nebe", 2],		
]

var data_cisla = [
	["strom", 0],
	["keř", 0],
	["stonek", 0],
	["kámen", 0],
	["vzduch", 0],
	["rybník", 0],
	["hrášek", 0],
	["písek", 0],
	["voda", 0],
	["květina", 0],	
	["ropa", 0],
	["jiřina", 0],
	["žížala", 0],
	["šiška", 0],
	["louka", 0],
	["bylina", 0],
	["kotě", 0],
	["kuře", 0],
	["poupě", 0],
	["semínko", 0],
	["mládě", 0],
	["zvíře", 0],	
	["štěně", 0],
	["kaštan", 0],
	["jahoda", 0],	
	
	["stromy", 1],
	["keře", 1],
	["kameny", 1],
	["plody", 1],
	["minerály", 1],
	["oceány", 1],
	["kocouři", 1],
	["psi", 1],
	["květiny", 1],
	["žížaly", 1],	
	["šišky", 1],
	["louky", 1],
	["větve", 1],
	["borůvky", 1],
	["papriky", 1],
	["kuřata", 1],
	["semínka", 1],
	["mláďata", 1],
	["kůzlata", 1],
	["poupata", 1],
	["jablka", 1],
	["koťata", 1],	
	["jiřiny", 1],
	["klíšťata", 1],
	["fazolky", 1],	
]
var questions = []

func generate_questions():
	for question in data:
		questions.append({
			"question": "Urči rod podstatného jména \"" + question[0] + "\"",
			"choices": rody,
			"correct": question[1]			
		})
		
	for question in data_cisla:
		questions.append({
			"question": "Urči číslo podstatného jména \"" + question[0] + "\"",
			"choices": cisla,
			"correct": question[1]			
		})
		
var question

# Called when the node enters the scene tree for the first time.
func _ready():
	generate_questions()
	set_random_question()
	
func set_random_question():
	question = questions[randi() % questions.size()] 
	$Question.text = question.question
	$Button1.hide()
	$Button2.hide()
	$Button3.hide()
	$Button1.modulate = Color("#ffffffff")
	$Button2.modulate = Color("#ffffffff")
	$Button3.modulate = Color("#ffffffff")
	
	for i in question.choices.size():
		get_node("Button" + str(i+1)).text = question.choices[i]
		get_node("Button" + str(i+1)).show()
	
func answer(idx):
	if question.correct == idx:
		$Sfx/correct.play()
		get_parent().correct_answer(self)
	else:
		$Sfx/incorrect.play()
		mark_incorrect(idx)
		get_parent().incorrect_answer(self)
	
	mark_correct()
		
	
func mark_correct():
	if has_node("Button" + str(question.correct + 1)):
		get_node("Button" + str(question.correct + 1)).modulate = Color("#78fa00")
	
	
func mark_incorrect(idx):
	if has_node("Button" + str(idx + 1)):
		get_node("Button" + str(idx + 1)).modulate = Color("#f80e00")
	
	
func _on_Button_pressed():
	answer(0)
	
func _on_Button2_pressed():
	answer(1)
	
func _on_Button3_pressed():
	answer(2)


func show_next_question():
	$Timer.start()

func _on_Timer_timeout():
	set_random_question()
