extends KinematicBody2D

export var SPEED = 30000

var motion = Vector2(0, 0)

func _ready():
	set_process(true)

func _process(delta):
	motion = Vector2(0,0)		
	if Input.is_action_pressed("ui_left"):
		motion.x -= SPEED * delta
		
	if Input.is_action_pressed("ui_right"):
		motion.x += SPEED * delta
		
	if Input.is_action_pressed("ui_up"):
		motion.y -= SPEED * delta
		
	if Input.is_action_pressed("ui_down"):
		motion.y += SPEED * delta
			
	motion = move_and_slide(motion)	
		
	

