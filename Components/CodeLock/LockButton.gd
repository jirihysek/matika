extends Area2D

export var label = "1"
export var value = 1

var lock

func _ready():
	lock = get_parent().get_parent().get_parent()
	$AudioStreamPlayer.pitch_scale = 1 + value * 0.03
	$Label.text = label

func _on_LockButton_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			$AnimationPlayer.stop()
			$Pressed.show()
			$AudioStreamPlayer.play()
			lock.pressed(value)
		else:
			$AnimationPlayer.play("Off")
