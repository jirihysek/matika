extends Area2D

export var value = 0

var locked = false
var lock 
var target = null

func _ready():
	lock = get_node("../../../..")

func lock_value(val):
	locked = true
	value = val
	$Label.text = str(val)
	$AnimationPlayer.play("Lock")
	
func clear():
	locked = false
	value = 0
	$Label.text = ""
	
	
func select():
	$Sprite.show()
	$AnimationPlayer.play("Selected")

func unselect():
	if not locked:
		$Sprite.hide()
		$AnimationPlayer.stop()
		if target and value == target:
			# TODO: effect
			lock_value(value)
	
func keyboard_enter(input_value):
	if input_value == -1:
		value = null
		$Label.text = ""
		
	var length = $Label.text.length()
	
	if length >= 3 or input_value < 0:
		print("ne")
		
	elif length >= 0:	
		$Label.text = $Label.text + str(input_value)	
		value = int($Label.text)
		
	if value == target:
		lock_value(value)
	
func _on_Number_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and not locked:
		if event.pressed:
			lock.select_number(self)
			select()
		else:
			pass
