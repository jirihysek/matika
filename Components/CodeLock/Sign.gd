extends Area2D

var signs = [
	"+",
	"-",
	"•",
	":"
]

export var sign_code = 0

func _ready():
	$Label.text = signs[sign_code]

