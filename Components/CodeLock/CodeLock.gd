extends Node2D

export var code_only = false

var selected
var unlocked = false
var door

enum SIGNS {
	plus,
	minus,
	times,
	divide
}

var numbers = [
	[3,5,1,2],
	[],
	[]
]
var signs = [
	[SIGNS.times, SIGNS.times],
	[SIGNS.plus]
]

func _ready():
	door = get_parent().get_parent()
	randomize()
	generate()
	recount()
	var node = $PanelLeft/Triangle/Numbers/Number5
	set_process_input(true)
	select_next_unlocked()
	if code_only:
		set_code_only()
	
func regenerate():
	var n5 = $PanelLeft/Triangle/Numbers/Number5
	var n6 = $PanelLeft/Triangle/Numbers/Number6
	var n7 = $PanelLeft/Triangle/Numbers/Number7
	n5.clear()
	n6.clear()
	n7.clear()
	
	
func set_code_only():
	code_only = true
	$PanelRight.hide()
	$Sticks.hide()

func solve(count):
	if count >= 1:
		var n5 = $PanelLeft/Triangle/Numbers/Number5
		n5.lock_value(n5.target)
		
	if count >= 2:
		var n6 = $PanelLeft/Triangle/Numbers/Number6
		n6.lock_value(n6.target)
	
	if count >= 3:
		var n7 = $PanelLeft/Triangle/Numbers/Number7
		n7.lock_value(n7.target)
			
	select_next_unlocked()
			
func _input(event):
	if event is InputEventKey and event.pressed:
		if Input.is_key_pressed(KEY_0):
			pressed(0)
		if Input.is_key_pressed(KEY_1):
			pressed(1)
		if Input.is_key_pressed(KEY_2):
			pressed(2)
		if Input.is_key_pressed(KEY_3):
			pressed(3)
		if Input.is_key_pressed(KEY_4):
			pressed(4)
		if Input.is_key_pressed(KEY_5):
			pressed(5)
		if Input.is_key_pressed(KEY_6):
			pressed(6)
		if Input.is_key_pressed(KEY_7):
			pressed(7)
		if Input.is_key_pressed(KEY_8):
			pressed(8)
		if Input.is_key_pressed(KEY_9):
			pressed(9)
		if Input.is_key_pressed(KEY_DELETE) or Input.is_key_pressed(KEY_BACKSPACE):
			pressed(-1)
		if Input.is_key_pressed(KEY_ENTER):
			pressed(-2)


func generate():
	numbers = [[]]
	
	var nums = $PanelLeft/Triangle/Numbers.get_children()
	
	for i in range(0,4):
		var rand = randi() % 8 + 2
		numbers[0].append(rand)
		nums[i].lock_value(rand)
			
	numbers.append([])
	numbers.append([])

func recount():
	var n5 = $PanelLeft/Triangle/Numbers/Number5
	var n6 = $PanelLeft/Triangle/Numbers/Number6
	var n7 = $PanelLeft/Triangle/Numbers/Number7
	n5.target = numbers[0][0] * numbers[0][1]
	n6.target = numbers[0][2] * numbers[0][3]
	n7.target = n5.target + n6.target
	
func all_locked():
	var n5 = $PanelLeft/Triangle/Numbers/Number5
	var n6 = $PanelLeft/Triangle/Numbers/Number6
	var n7 = $PanelLeft/Triangle/Numbers/Number7
	return n5.locked and n6.locked and n7.locked	
		
		
func select_number(node):
	for number in $PanelLeft/Triangle/Numbers.get_children():
		number.unselect()	
	node.select()
	selected = node

func select_next_unlocked():
	var n5 = $PanelLeft/Triangle/Numbers/Number5
	var n6 = $PanelLeft/Triangle/Numbers/Number6
	var n7 = $PanelLeft/Triangle/Numbers/Number7
	
	if !n5.locked:
		select_number(n5)
		return
	
	if !n6.locked:
		select_number(n6)
		return
			
	if !n7.locked:
		select_number(n7)
		return
	
func pressed(value):
	if value == -2:
		var was_selected = selected
		
		selected = null
		for number in $PanelLeft/Triangle/Numbers.get_children():
			number.unselect()
		
		if all_locked():
			unlock()
		else:
			if was_selected and was_selected.locked:
				select_next_unlocked()	
		return
		
	if selected:
		selected.keyboard_enter(value)
		if selected.locked:
			if all_locked():
				unlock()
			else:
				select_next_unlocked()


func unlock():
	if !unlocked:
		unlocked = true
		$Sfx/Unlock.play()
		$AnimationPlayer.play("Unlock")
		$Timer.start()	

func _on_Timer_timeout():
	door.solve_puzzle()
