extends Area2D

export var lock_on_teleport = ""

var disabled = false

func _on_Teleport_body_entered(body):
	if !disabled and body.is_in_group("Player"):
		get_node("/root/Game/Sfx/Teleport").play()
		body.position = $Target.global_position
		if lock_on_teleport != "":
			var door = get_node("/root/Game/Environment/" + lock_on_teleport)
			door.reenable()
			
func disable():
	disabled = true
	$Sprite.hide()
	
func enable():
	disabled = false
	$Sprite.show()
