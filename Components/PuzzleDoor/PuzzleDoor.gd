extends Area2D

export var title = "Dveře jsou zamknuty znalostním kódem."
export var type = "door"
export var puzzle_type = "CodeLock"
export var already_solved = 0
export var walkthrough = false
export var switch_on_unlock = ""
export var autoclose = true
export var tutorial_sound = ""

var solved = false

func _ready():
	$PuzzleBox/Title.text = title
	for node in $Visual.get_children():
		node.hide()
	get_node("Visual/" + type).show()
	
	if walkthrough:
		$StaticBody2D.queue_free()
		
	if type == "lever":
		$PuzzleBox/CodeLock.set_code_only()

	$PuzzleBox/CodeLock.hide()
	$PuzzleBox/TestQuestion.hide()
	$PuzzleBox/GridLock.hide()
	$PuzzleBox/GridLockLarge.hide()
		
	if puzzle_type == "CodeLock":
		$PuzzleBox/CodeLock.show()
		$PuzzleBox/CodeLock.solve(already_solved)
		
	if puzzle_type == "GridLock":
		$PuzzleBox/GridLock.show()
		$PuzzleBox/GridLock.solve(already_solved)

	if puzzle_type == "GridLockLarge":
		$PuzzleBox/GridLockLarge.show()
		$PuzzleBox/GridLockLarge.solve(already_solved)
		
	if puzzle_type == "TestQuestion":
		$PuzzleBox/TestQuestion.show()
		
		
func _on_PuzzleDoor_body_entered(body):
	if !solved and body.is_in_group("Player"):
		if tutorial_sound == "1":
			$Sfx/s1.play()
			
		if tutorial_sound == "2":
			$Sfx/s2.play()
			
		open_puzzle()

func _on_PuzzleDoor_body_exited(body):
	if !solved and body.is_in_group("Player"):
		close_puzzle()		
		
func open_puzzle():
	var cam = get_node("/root/Game/Player/Camera2D")
	cam.global_position = $PuzzleBox/CameraLock.global_position
	$AnimationPlayer.play("ShowPuzzleBox")

func close_puzzle():
	get_node("/root/Game/Player/Camera2D").position = Vector2(0,0)
	$AnimationPlayer.play_backwards("ShowPuzzleBox")

func solve_puzzle():
	solved = true
	if !walkthrough:
		$StaticBody2D.collision_mask = 0
		$StaticBody2D.collision_layer = 0
		
	close_puzzle()
	get_node("Visual/" + type + "/SpriteLocked").hide()
	get_node("Visual/" + type + "/SpriteSolved").show()

	if switch_on_unlock != "":
		get_node("/root/Game/" + switch_on_unlock).disable()	

func reenable():
	solved = false
	if has_node("StaticBody2D"):
		$StaticBody2D.collision_layer = 1
		$StaticBody2D.collision_mask = 1
	#$PuzzleBox/CodeLock.regenerate()
	#$PuzzleBox/GridLock.regenerate()
	get_node("Visual/" + type + "/SpriteLocked").show()
	get_node("Visual/" + type + "/SpriteSolved").hide()
	

func _on_OpenTimer_timeout():
	pass
	#	reenable()
