extends Panel

var correct_count = 0

func correct_answer(question_box):
	correct_count += 1
	if correct_count >= 3:
		get_parent().solve_puzzle()
		question_box.set_random_question()
	else:
		question_box.show_next_question()
	
func incorrect_answer(question_box):
	question_box.show_next_question()
