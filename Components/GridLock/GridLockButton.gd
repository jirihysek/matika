extends Area2D

export var label = "1"
export var grid_x = 0
export var grid_y = 0
export var value = 1

var lock
var locked = false

func _ready():
	lock = get_parent().get_parent().get_parent()
	$AudioStreamPlayer.pitch_scale = 1 + grid_x * grid_y * 0.03
	$Label.text = "?"

func grid_vec():
	return Vector2(grid_x, grid_y)

func set_number(num):
	value = num
	label = str(num)
	$Label.text = str(num)

func select():
	$AnimationPlayer.stop()
	$Pressed.show()
	$Pressed.self_modulate = Color(1,1,1,1)
	
func unselect():
	if !locked:
		$AnimationPlayer.play("Off")

func lock():
	locked = true
	
func _on_LockButton_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		if event.pressed:
			$AudioStreamPlayer.play()
			select()
			lock.pressed(self)
