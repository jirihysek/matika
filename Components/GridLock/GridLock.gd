extends Node2D

export var code_only = false
export var large = false

var selected = []
var door
var size = 4
var results = 3

var DIRECTIONS = [
	Vector2(0, -1),
	Vector2(1, 0),
	Vector2(0, 1),
	Vector2(-1, 0)
]

var numbers = []
var solution = []

func _ready():
	if large:
		size = 5
		$PanelLeft.hide()
		$PanelLeft5x5.show()
	else:
		size = 4
		$PanelLeft.show()
		$PanelLeft5x5.hide()
		
	door = get_parent().get_parent()
	randomize()
	generate_grid()
	generate_solution()
	if code_only:
		set_code_only()
	
func set_code_only():
	code_only = true
	$PanelRight.hide()
	$Sticks.hide()

func regenerate():
	generate_grid()
	generate_solution()

func solve(count):
	for i in range(0, count):
		$PanelRight/Locks.get_node("Lock" + str(i+1)).self_modulate = Color("7eb500")
		solution[i].solved = true
		var btn = $PanelLeft/Keyboard.get_node("Btn" + str(solution[i].pair[0].x * 4 + solution[i].pair[0].y + 1))
		btn.select()
		btn.lock()
		btn = $PanelLeft/Keyboard.get_node("Btn" + str(solution[i].pair[1].x * 4 + solution[i].pair[1].y + 1))
		btn.select()
		btn.lock()

func generate_grid():
	numbers = []
		
	for i in range(0,size):
		var line = []
		for j in range(0,size):
			var num = randi() % 4 + 2
			line.append(num)		
			var btn
			if large:
				btn = $PanelLeft5x5/Keyboard.get_node("Btn" + str((i * size) + j + 1))
			else:
				btn = $PanelLeft/Keyboard.get_node("Btn" + str((i * 4) + j + 1))
			btn.set_number(num)
			
		numbers.append(line)
		
func generate_solution():
	var pair
	
	for i in range(0, results):
		while true:
			pair = generate_pair()
			if !collides_with_solution(pair):
				break
		var result = numbers[pair[0].x][pair[0].y] * numbers[pair[1].x][pair[1].y]
		$PanelRight/Locks.get_node("Lock" + str(i + 1) + "/Label").text = str(result)
		solution.append({ "result": result, "pair": pair, "solved": false })		
			
	
func generate_pair():
	var starting_point = random_grid_starting_point()
	var rand_direction = randi() % 4
	 
	while !is_valid_direction(starting_point, DIRECTIONS[rand_direction]):
		rand_direction = (rand_direction + 1) % 4
			
	return [starting_point, starting_point + DIRECTIONS[rand_direction]]


func collides_with_solution(pair):
	for s in solution:
		if s.pair[0] == pair[0] or s.pair[0] == pair[1] or s.pair[1] == pair[0] or s.pair[1] == pair[1]:
			return true
	return false

func is_valid_direction(from, direction):
	var res = from + direction
	return res.x >= 0 and res.x <= (size - 1) and res.y >= 0 and res.y <= (size - 1)		


func random_grid_starting_point():
	var x = randi() % size
	var y = randi() % size
	return Vector2(x, y)
		
func check_solution():
	for s in solution:
		if !s.solved:
			return false
			
	unlock()
	return true
		
func pressed(btn):
	selected.append(btn)
	
	if selected.size() == 2:
		if neighbours(selected[0].grid_vec(), selected[1].grid_vec()): 
			var v1 = selected[0].value
			var v2 = selected[1].value
			var i = 0
			for s in solution:
				i = i + 1
				if not s.solved:
					if s.result == v1 * v2:
						$Sfx/Unlock.play()
						$PanelRight/Locks.get_node("Lock" + str(i)).self_modulate = Color("7eb500")
						selected[0].lock()
						selected[1].lock()
						s.solved = true
						selected = []
						check_solution()
						return
			
		selected[0].unselect()
		selected[1].unselect()
		selected = []
		
func neighbours(b1: Vector2, b2: Vector2):
	return b1.distance_to(b2) == 1
		
func unlock():
	$Sfx/Open.play()
	$AnimationPlayer.play("Unlock")
	$Timer.start()	

func _on_Timer_timeout():
	door.solve_puzzle()
