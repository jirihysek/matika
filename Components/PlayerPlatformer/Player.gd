extends KinematicBody2D

export var SPEED = 25000
var GRAVITY = 2000
var JUMP_SPEED = 700

var motion = Vector2(0, 0)

func _ready():
	set_process(true)

func _process(delta):
	motion.y += delta * GRAVITY
	
	if Input.is_action_pressed("ui_left"):
		motion.x = -SPEED * delta
		
	elif Input.is_action_pressed("ui_right"):
		motion.x = SPEED * delta
		
	else:
		motion.x = 0
		
	if Input.is_action_just_pressed("ui_up") and is_on_floor():
		motion.y -= JUMP_SPEED
	
	motion = move_and_slide(motion, Vector2.UP)
