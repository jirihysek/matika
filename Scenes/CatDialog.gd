extends Node2D


var can_win = true

	
func start():
	var player = get_node("/root/Game/Player")
	player.paused = true
	player.get_node("AnimationPlayer").stop()
	player.get_node("AnimationPlayer").play("Idle")
	player.get_node("Sfx/Run").stop()
	var cam = player.get_node("Camera2D")
	cam.global_position = $CameraLock.global_position
	
	$AnimationPlayer.play("fadein")
	$MsgFinish.show()	

func _on_Yes_pressed():
	$Msg1.hide()
	$Question.show()
	$Question/TextEdit.focus_mode = Control.FOCUS_ALL


func _on_No_pressed():
	can_win = false
	$Msg1.hide()
	$Msg2.show()
	
func _on_OK_pressed():
	$Msg2.hide()
	$Question.show()


func _on_OK2_pressed():
	var player = get_node("/root/Game/Player")
	$Question.hide()
	$Msg2.hide()
	$Msg1.hide()
	if $Question/TextEdit.text == "84":
		if can_win:
			get_node("/root/Game").save_result()
			$MsgFinish/Label.text = "Skvěle! Někoho jako jsi ty potřebuju! Od teď budeš mým pomocníkem.\n\nTvůj čas: " + str(round(player.time)) + " sekund"
		else:
			$MsgFinish/Label.text = "No dobře, nechám tě jít. Tak běž než si to rozmyslím."
	else:
		if can_win:
			$MsgFinish/Label.text = "Ne, asi jsem se v tobě spletl, ale dám ti ještě šanci, pokud znovu dojdeš až sem."
		else:
			$MsgFinish/Label.text = "Špatně! Chramst... "
			get_node("/root/Game/Player").hide()
		
	$MsgFinish.show()
			
			
