extends Node2D

var player_name = ""
export var for_html = false
export var autostart = false

func _ready():
		
	if get_viewport().size.x > 2000:
		for_html = true
		
	if for_html:
		$Player/Camera2D.zoom = Vector2(0.5, 0.5)
		$UI/Control/Start.rect_scale = Vector2(2, 2)
	else:
		$Player/Camera2D.zoom = Vector2(1, 1)
		$UI/Control/Start.rect_scale = Vector2(1, 1)

	if autostart:
		$UI/Control/Start.hide()
		$Player.paused = false
		$Music.play()
	
func start():
	var nick = $UI/Control/Start/LineEdit.text
	if nick == "":
		$UI/Control/Start/AnimationPlayer.play("nene")
	else:
		$UI/Control/Start.hide()
		player_name = nick
		$Player.paused = false
		$Music.play()

func _on_FinishRight_body_entered(body):
	if body.is_in_group("Player"):
		$CatDialog.start()
		$Environment/Cat/AnimationPlayer2.play("fall_left")
		$Environment/FinishRight.queue_free()


func _on_FinishLeft_body_entered(body):
	if body.is_in_group("Player"):
		$CatDialog.start()
		$Environment/Cat/AnimationPlayer2.play("fall_right")
		$Environment/FinishLeft.queue_free()

func _on_OK_pressed():
	get_tree().change_scene("res://Scenes/Game.tscn")


func _on_No_pressed():
	get_tree().quit()

func _on_Button_pressed():
	start()
	
func save_result():
	var data = {
		"player": player_name,
		"score_label": str(round($Player.time)) + "s",
		"score_value": str(10000 - $Player.time),
		"token": "QRpIQngUVEacYPxJW8UbDryjvbAMZEHcw"
	}
	$HTTPRequest.request("https://leaderboards.herokuapp.com/scores", ["Content-Type: application/json"], true, HTTPClient.METHOD_POST, JSON.print(data))

	
func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var data = parse_json(body.get_string_from_utf8())
	var list = $UI/Control/Leaderboard/ItemList
	for score in data.leaderboard:
		list.add_item(score.player + "      ")
		list.add_item(score.score_label)
	$UI/Control/Leaderboard.show()

func _on_CloseLeaderboard_pressed():
	$UI/Control/Leaderboard.hide()
